package com.thoughtworks.vapasi;

import java.util.HashMap;
import java.util.Objects;

public class Quantity {

    double value;
    Unit unit;
    Quantity(double value, Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    public Quantity meter(double i) {
        this.unit = new Unit("length", "meter");
        return new Quantity(i, unit);
    }

    public Quantity centimeter(double i) {
        this.unit = new Unit("length", "centimeter");
        return new Quantity(i, unit);
    }

    public Quantity  kilometer(double i) {
        this.unit = new Unit("length", "kilometer");
        return new Quantity(i, unit);
    }
    public Quantity  gram(double i) {
        this.unit = new Unit("weight", "gram");
        return new Quantity(i, unit);
    }
    public Quantity  kilogram(double i) {
        this.unit = new Unit("weight", "kilogram");
        return new Quantity(i, unit);
    }
    public Quantity add(Quantity secondQuantity) throws Exception {
        if(!this.unit.type.equals(secondQuantity.unit.type)){
                throw new IllegalArgumentException();
        }
        double firstValueInBase = this.unit.convertToBase(this);
        double secondValueInBase = secondQuantity.unit.convertToBase(secondQuantity);
        double sum = firstValueInBase + secondValueInBase;
        Quantity newQuantityInBase = new Quantity(sum, new Unit("length", "meter"));
        double addedValueInFirstUnit = this.unit.convertFromBase(newQuantityInBase, this.unit);
        return new Quantity(addedValueInFirstUnit, this.unit);
    }
    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Quantity other = (Quantity)o;
        if(!this.unit.type.equals(other.unit.type))
            return false;

        double firstValueInBase = unit.convertToBase(this);
        double secondValueInBase = other.unit.convertToBase(other);
        return firstValueInBase==firstValueInBase;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, unit);
    }
}



