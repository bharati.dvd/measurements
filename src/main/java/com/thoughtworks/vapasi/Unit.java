package com.thoughtworks.vapasi;
import java.util.*;
public class Unit {

        String type;
        String name;

        Map<String, Double> map = new HashMap<String, Double>();
        Map<String, Double> map2 = new HashMap<String, Double>();

        public Unit(String type,String name) {
            this.type = type;
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            return (this.type.equals(((Unit)o).type));
        }

        public double convertToBase(Quantity q)
        {
            if(q.unit.name.equals("meter"))
                return q.value;
            this.convertToBaseInit();
            return q.value * map.get(q.unit.name);
        }
        void convertToBaseInit(){
            map.put("millimeter", 0.001);
            map.put("centimeter", 0.01);
            map.put("kilometer", 1000.0);
        }
        public double convertFromBase(Quantity q, Unit newUnit)
        {
            this.convertFromBaseInit();
            return q.value * map2.get(newUnit.name);
        }
        void convertFromBaseInit(){
            map2.put("millimeter", 1000.0);
            map2.put("centimeter", 100.0);
            map2.put("kilometer", 0.001);
        }
        Quantity gramToKilogram(Quantity q) throws IllegalArgumentException{
            if(!q.unit.type.equals("weight") || !q.unit.name.equals("gram")) {
                throw new IllegalArgumentException();
            }
            Unit unit = new Unit(q.unit.type, "kilogram");
            return new Quantity(q.value/1000.0, unit);
        }
        Quantity kiloGramToGram(Quantity q) throws IllegalArgumentException{
            if(!q.unit.type.equals("weight") || !q.unit.name.equals("kilogram")) {
                throw new IllegalArgumentException();
            }
            Unit unit = new Unit(q.unit.type, "gram");
            return new Quantity(q.value*1000.0, unit);
        }
}