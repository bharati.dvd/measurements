package com.thoughtworks.vapasi;
import jdk.jfr.Threshold;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
public class QuantityTest {

    @Test
    void shouldReturnTrueIfValuesAreEqual()
    {
        Assertions.assertEquals(new Quantity(1,new Unit("length", "meter")),new Quantity(100,new Unit("length", "centimeter")));
    }
    @Test
    void shouldReturnTrueIfValuesAreNotEqual()
    {
        Assertions.assertEquals(new Quantity(1,new Unit("length", "meter")),new Quantity(105,new Unit("length", "centimeter")));
    }
    @Test
    public void checkConvertIntoBaseValue(){
        Unit unit = new Unit("length", "centimeter");
        Quantity q = new Quantity(100, unit);
        Assertions.assertEquals(1, unit.convertToBase(q));
    }
    @Test
    public void shouldMakeTheUnitMeter(){
        Unit unit = new Unit("length", "meter");
        Quantity q = new Quantity(10, unit);
        Assertions.assertEquals("meter", q.unit.name);
    }
    @Test
    public void shouldMakeTheUnitCentimeter(){
        Unit unit = new Unit("length", "centimeter");
        Quantity q = new Quantity(104, unit);
        Assertions.assertEquals("centimeter", q.unit.name);
    }
    @Test
    public void shouldMakeTheUnitKilometer(){
        Unit unit = new Unit("length", "kilometer");
        Quantity q = new Quantity(10, unit);
        Assertions.assertEquals("kilometer", q.unit.name);
    }
    @Test
    public void shouldMakeTheUnitKilogram(){
        Unit unit = new Unit("weight", "kilogram");
        Quantity q = new Quantity(10, unit);
        Assertions.assertEquals("kilogram", q.unit.name);
    }
    @Test
    public void shouldMakeTheUnitGram(){
        Unit unit = new Unit("weight", "gram");
        Quantity q = new Quantity(10, unit);
        Assertions.assertEquals("gram", q.unit.name);
    }
    @Test
    public void shouldAddTwoQuantitiesAndReturnInFirstUnitsOfFirstQuantity() throws Exception {
        Unit firstUnit = new Unit("length", "centimeter");
        Quantity firstQuantity = new Quantity(500, firstUnit);
        Unit secondUnit = new Unit("length", "meter");
        Quantity secondQuantity = new Quantity(1, secondUnit);

        Assertions.assertEquals(600, firstQuantity.add(secondQuantity).value);
    }
    @Test
    public void shouldThrowIllegalArgumentException() throws Exception {
        Unit firstUnit = new Unit("weight", "gram");
        Quantity firstQuantity = new Quantity(500, firstUnit);
        Unit secondUnit = new Unit("length", "meter");
        Quantity secondQuantity = new Quantity(1, secondUnit);

        Assertions.assertThrows(IllegalArgumentException.class,
                ()-> firstQuantity.add(secondQuantity) );
    }
}

