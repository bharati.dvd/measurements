package com.thoughtworks.vapasi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UnitTest {
    @Test
    void shouldReturnTrueIfTypesAreEqual()
    {
        Assertions.assertEquals(true, new Unit("length", "meter").equals(
                new Unit("length", "meter")
        ));
    }
    @Test
    void shouldReturnFalseIfTypesAreNotEqual()
    {
        Assertions.assertEquals(false, new Unit("length", "meter").equals(
                new Unit("weight", "kilogram")
        ));
    }
    @Test
    void shouldConvertIntoBase(){
        Unit unit = new Unit("length", "kilometer");
        Quantity q = new Quantity(2, unit);
        Assertions.assertEquals(2000, q.unit.convertToBase(q));
    }

    @Test
    void shouldConvertFromBase(){
        Unit unit = new Unit("length", "meter");
        Quantity q = new Quantity(5000, unit);
        Unit newUnit = new Unit("length", "kilometer");
        Assertions.assertEquals(5, q.unit.convertFromBase(q, newUnit));
    }
    @Test
    public void shouldConvertGramToKilogram(){
        Quantity q = new Quantity(2000, new Unit("weight", "gram"));
        Assertions.assertEquals(2, q.unit.gramToKilogram(q).value);
    }

    @Test
    public void shouldConvertKilogramToGram(){
        Quantity q = new Quantity(7, new Unit("weight", "kilogram"));
        Assertions.assertEquals(7000, q.unit.kiloGramToGram(q).value);
    }
    @Test
    public void shouldThrowIllegalArgumentException() throws Exception {
        Quantity q = new Quantity(7, new Unit("weight", "kilogram"));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> q.unit.gramToKilogram(q));
    }
}
